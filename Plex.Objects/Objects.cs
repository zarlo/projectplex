using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plex.Objects
{
    public class GUIDRequest
    {
        public string name { get; set; }
        public string guid { get; set; }
    }
}
